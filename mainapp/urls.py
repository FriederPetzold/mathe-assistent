from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.home_page_view, name='landingpage'),
    path('md_inhalt/<str:content_code>', views.md_inhalt_view, name='md_inhalt'),
    path('impressum/<str:page_code>', views.imprint_view, name='imprint'),
    path('overview/<str:page_code>', views.overview_view, name='overview'),
    path(r'debug', views.debug_view, name='debugpage0'),
    path(r'debug/<int:xyz>', views.debug_view, name='debugpage_with_argument'),
]
