---
content_type: article
page_id: koZa_ReOp_Betrag
page_name: Betrag von komplexen Zahlen
main_category: kat_komplex_Zahlen
sub_category: kat_ReOp_komplex_Zahlen
requirements: 
exercise_path:
---

## Der Betrag von komplexen Zahlen

Da eine komplexe Zahl die Form $z =a+bi$ hat, ist ihr Wert also von zwei Parametern abhängig. Dies macht es nicht möglich, komplexe Zahlen nach ihrer "Größe" wie natürliche Zahlen zu vergleichen. 

Dennoch lassen sich Aussagen über die Entfernung einer komplexen Zahl zum Koordinatenursprung treffen. Dafür wird der Betrag genutzt. Er wird über den Satz des Pythagoras berechnet.

<div class="col-container" markdown="1">
  <div class="col" markdown="1">

**Allgemeiner Fall**

---

$z = a + bi$

${|z|}^2 = a^2+b^2$

$|z| = \sqrt{a^2 + b^2}$

  </div>
  <div class="col col-100" markdown="1">

**Beispiel**

---

$z = 4 + 3i$

${|z|}^2 = 4^2+3^2$

$|z| = \sqrt{4^2 + 3^2}$

$|z| = \sqrt{25}$

$|z| = 5$

  </div>
</div>
