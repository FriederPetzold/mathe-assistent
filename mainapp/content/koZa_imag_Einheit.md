---
content_type: article
page_id: koZa_imag_Einheit
page_name: Die imaginäre Einheit i
main_category: kat_komplex_Zahlen
sub_category: kat_Darstellung_komplex_Zahlen
requirements: 
exercise_path: imaginaere_Einheit
---

# Die imaginäre Einheit i

Für die mathematische Beschreibung wird dafür die imaginäre Einheit **i** definiert, so dass
$i^2 = -1$
gilt.

Mit dieser Definition lassen sich auch die Wurzeln von negativen Zahlen ausrechnen:



| Allgemein |Beispiel |
| -------- | -------- |
| $\sqrt{-x} = \sqrt{i^2 \cdot x} = i \cdot \sqrt{x}$     | $\sqrt{-2} = \sqrt{i^2 \cdot 2} = i \cdot \sqrt2$     |


