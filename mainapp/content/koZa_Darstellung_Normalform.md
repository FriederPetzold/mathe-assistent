---
content_type: article
page_id: koZa_Darstellung_Normalform
page_name: Darstellung von komplexen Zahlen in der Normalform
main_category: kat_komplex_Zahlen
sub_category: kat_Darstellung_komplex_Zahlen
requirements: koZa_imag_Einheit
exercise_path: 
---

# Darstellung von komplexen Zahlen in der Normalform

Das in [der Einführung](koZa_Einführung) angesprochene Problem, das berechnen von Wurzeln von nagativen Zahlen, kann durch die [imaginäre Einheit i](koZa_imag_Einheit) gelöst werden. Dennoch bestehen die dortigen Lösungen aus zwei Summanden wie zum Beispiel  $\left(2 + \sqrt{-2}\right)$.

Dies kann zwar noch mit der imaginären Einheit zu $\left(2 + i·\sqrt{2}\right)$ vereinfacht werden, dennoch bleibt aufgrund des Unterschiedes zwischen einem Vielfachen einer rationalen Zahl und dem Vielfachen der imaginären Einheit dei Summe bestehen.

**Eine Komplexe Zahl in der Normalform besteht somit immer aus zwei Summanden**

Die kompelxe Zahl $z$ wird dann durch die Summe $z = a + b·i$ dargestellt. $a$ gibt dabei den reellen Tei der Summe an und wird deshalb auch Realteil genannt. $b$ gibt das Vielfache der imaginären Einheit an, deshalb wird $b·i$ auch Imaginärteil genannt.
