import random
try:
    # direktes Importieren aus dem gleichen Verzeichnis
    from Formelformatierung import change, pz
except ImportError:
    # absoluten import-Pfad nutzen
    # setzt voraus, dass das Paket im PYTHONPATH findbar ist (z.b. über sy.path.append)
    from mainapp.content.exercise_generators.Formelformatierung import change, pz
import math


def getAufgabe():
    i = random.randint(0, 100)
    ineu = i
    teiler = []
    for t in [100, 81, 64, 49, 36, 25, 16, 9, 4]:
        if ineu % t == 0:
            teiler.append(t)
            ineu = int(ineu / t)

    res = f"""
*Berechne den Wert von $\sqrt[-{i}]$ !*


#### Lösung:

$\sqrt[{-i}]$

$\Leftrightarrow \sqrt[i^2·{i}]$

$\Leftrightarrow i·"""

    for t in teiler:
        res += str(int(math.sqrt(t))) + "·"

    end = f"""
\sqrt[{ineu}]$
"""
    res += end

    return change(res)
