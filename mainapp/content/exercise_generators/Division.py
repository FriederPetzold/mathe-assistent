import random
try:
    # direktes Importieren aus dem gleichen Verzeichnis
    from Formelformatierung import change, pz, internerLink
except ImportError:
    # absoluten import-Pfad nutzen
    # setzt voraus, dass das Paket im PYTHONPATH findbar ist (z.b. über sy.path.append)
    from mainapp.content.exercise_generators.Formelformatierung import change, pz, internerLink

def getAufgabe():
    i = 1j

    sol = random.randint(0, 100) + random.randint(0, 100) * i

    a2 = random.randint(0, 100)
    b2 = random.randint(0, 100)

    z2 = a2 + b2 * i
    z1 = sol * z2

    a1 = str(int(z1.real))
    b1 = str(int(z1.imag))
    a2 = str(int(a2))
    b2 = str(int(b2))

    res = f"""
*Berechne die Lösung von*
$[{pz(z1) + "/" + pz(z2)}]$

#### Lösung

**Kurzfassung**

$[{pz(z1) + "/" + pz(z2)}] = [[{a1 + "*" + a2 + "+" + b1 + "*" + b2}]/[{a2}^2+{b2}^2]]+[[{a2 + "*" + b1 + "-" + a1 + "*" + b2}]/[{a2}^2+{b2}^2]]i$

$\Leftrightarrow {pz(z1 / z2)}$

**Ausführliche Lösung**

$[{a1 + "+" + b1}i]/[{a2 + "+" + b2}i]$

$\Leftrightarrow [[({a1 + "+" + b1}i)*({a2 + "-" + b2}i)]/[({a2 + "+" + b2}i)*({a2 + "-" + b2}i)]]$

$\Leftrightarrow [[({a1}*{a2}+{b1}*{b2})+(-{a1}*{b2}+{a2}*{b1})]/[{a2 + "^2-" + b2}]^2]$
[Multiplikation komplexer Zahlen]({internerLink("Multiplikation")})

$\Leftrightarrow [[{int(a1) * int(a2)}+{int(b1) * int(b2)}]/[{int(a2) * int(a2)}+{int(b2) * int(b2)}]] + [[(-{int(a1) * int(b2)}+{int(a2) * int(b1)})i]/[{int(a2) * int(a2)}+{int(b2) * int(b2)}]]$

$\Leftrightarrow {pz(z1 / z2)}$
"""

    return(change(res))
