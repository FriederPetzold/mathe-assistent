try:
    # direktes Importieren aus dem gleichen Verzeichnis
    from Formelformatierung import change, pz
except ImportError:
    # absoluten import-Pfad nutzen
    # setzt voraus, dass das Paket im PYTHONPATH findbar ist (z.b. über sy.path.append)
    from mainapp.content.exercise_generators.Formelformatierung import change, pz
import random


def getAufgabe():
    i = 1j

    # random.seed(1)
    a1 = random.randint(-100, 100)
    a2 = random.randint(-100, 100)
    b1 = random.randint(-100, 100)
    b2 = random.randint(-100, 100)

    z1 = a1 + b1 * i
    z2 = a2 + b2 * i
    sol = z1 * z2

    a1 = str(a1)
    a2 = str(a2)
    b1 = str(b1)
    b2 = str(b2)

    res = f"""
*Berechne den Wert von*
$({pz(z1) + ")*(" + pz(z2)})$


#### Lösung
**Kurzfassung**

$({a1 + "+" + b1}i)*({a2 + "+" + b2}i) = ({a1 + "*" + a2 + "-" + b1 + "*" + b2})+({a1 + "*" + b2 + "+" + b1 + "*" + a2})i = {pz(sol)}$


**Ausführliche Lösung**

$({pz(z1) + ")*(" + pz(z2)})$

$\Leftrightarrow {a1 + "*" + a2 + "+" + a1 + "*" + b2 + "i" + "+" + b1 + "i*" + a2 + "+" + b1 + "i*" + b2 + "i"}$

$\Leftrightarrow {str(int(a1) * int(a2)) + "+" + str(int(a1) * int(b2)) + "i" + "+" + str(int(b1) * int(a2)) + "i" + "-" + str(int(b1) * int(b2))}$ 

$\Leftrightarrow ({str(int(a1) * int(a2)) + "-" + str(int(b1) * int(b2)) + ")" + "+" + "(" + str(int(a1) * int(b2)) + "+" + str(int(b1) * int(a2))})i$

$\Leftrightarrow {pz(sol)}$
"""

    return change(res)
