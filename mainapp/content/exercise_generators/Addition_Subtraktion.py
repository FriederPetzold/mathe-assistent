#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 13:47:28 2021

@author: frieder
"""

import random
try:
    # direktes Importieren aus dem gleichen Verzeichnis
    from Formelformatierung import change, pz
except ImportError:
    # absoluten import-Pfad nutzen
    # setzt voraus, dass das Paket im PYTHONPATH findbar ist (z.b. über sy.path.append)
    from mainapp.content.exercise_generators.Formelformatierung import change, pz


def getAufgabe():
    i = 1j
    # random.seed(1)
    a1 = random.randint(-100, 100)
    a2 = random.randint(-100, 100)
    b1 = random.randint(-100, 100)
    b2 = random.randint(-100, 100)

    z1 = a1 + b1 * i
    z2 = a2 + b2 * i
    op = random.choice(["+", "-"])
    sol = 0

    if op == "+":
        sol = z1 + z2
    elif op == "-":
        sol = z1 - z2

    res = f""" 
*Berechne den Wert von:* $({pz(z1) + ")" + op + "(" + pz(z2)})$



#### Lösung

**Kurzfassung**

$({pz(z1) + ")" + op + "(" + pz(z2)}) = ( {str(a1) + op + str(a2)} ) + ({str(b1) + op + str(b2)}) i = {pz(sol)}$



**Ausführliche Lösung**

$({pz(z1) + ")" + op + "(" + pz(z2)})$

$\Leftrightarrow	 {str(a1) + "+" + str(b1) + "i" + op + str(a2) + op + str(b2) + "i"}$

$\Leftrightarrow	 ({str(a1) + op + str(a2) + ")" + "+" + "(" + str(b1) + op + str(b2)})i$

$\Leftrightarrow	 {pz(sol)}$

---
"""

    return change(res)
