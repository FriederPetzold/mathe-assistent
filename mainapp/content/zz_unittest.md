---
content_type: article
page_id: zz_unittest
page_name: zz_unitest
main_category: 
sub_category: 
requirements: koZa_ReOp_Multiplikation
              koZa_ReOp_Multiplikation
exercise_path: 
---

<!-- utc_md_unittest_content -->
<!-- Anzahl der erwarteten Menüpunkte: 4 --> 


<div markdown="1">

**unittest_strong**

</div>


$\Leftrightarrow  \left( a_1 + b_1 i \right) \cdot  \left( a_2 + b_2 i \right)$

Diese Datei ist eine Testatei
Sie dient dazu, das korrekte Markdown durch unittest zu überprüfen


