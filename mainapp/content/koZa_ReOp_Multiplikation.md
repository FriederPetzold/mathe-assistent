---
content_type: article
page_id: koZa_ReOp_Multiplikation
page_name: Multiplikation komplexer Zahlen
main_category: kat_komplex_Zahlen
sub_category: kat_ReOp_komplex_Zahlen
requirements: koZa_ReOp_Addition_Subtraktion
exercise_path: Multiplikation
---

# Multiplikation komplexer Zahlen
---

<div class="col-container" markdown="1">
  <div class="col" markdown="1">

**Allgemeiner Fall**

---

$z_1  \cdot  z_2$

$\Leftrightarrow  \left( a_1 + b_1 i \right) \cdot  \left( a_2 + b_2 i \right)$

$\Leftrightarrow a_1  \cdot  a_2 + a_1 \cdot b_2 \cdot i + b_1 \cdot i \cdot a_2 + b_1  \cdot i \cdot b_2 \cdot i$

$\Leftrightarrow a_1  \cdot  a_2 + a_1 \cdot b_2 \cdot i + b_1 \cdot i \cdot a_2 - b_1  \cdot b_2$

$\Leftrightarrow  \left( a_1  \cdot  a_2 - b_1  \cdot b_2 \right) +  \left( a_1 \cdot b_2 + b_1 \cdot a_2 \right) \cdot i$
([Addition und Subtraktion komplexer Zahlen](koZa_ReOp_Addition_Subtraktion))

  </div>

  <div class="col col-100" markdown="1">

**Beispiel**

---

$~$

$\left( 5+7i \right) \cdot \left( 3+2i \right)$

$\Leftrightarrow 5 \cdot 3+5 \cdot 2i+7i \cdot 3+7i \cdot 2i$

$\Leftrightarrow 15+10i+21i-14$ 

$\Leftrightarrow \left( 15-14 \right)+\left( 10+21 \right)i$
([Addition und Subtraktion komplexer Zahlen](koZa_ReOp_Addition_Subtraktion))

$\Leftrightarrow 1+31i$

  </div>
</div> 
    

