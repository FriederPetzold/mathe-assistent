---
content_type: article
page_id: koZa_ReOp_Addition_Subtraktion
page_name: Addition und Subtraktion komplexer Zahlen
main_category: kat_komplex_Zahlen
sub_category: kat_ReOp_komplex_Zahlen
requirements:
exercise_path: Addition_Subtraktion
---

# Adition und Subtraktion von komplexen Zahlen

### Addition
Beispiel | allgemeiner Fall
---	| ---
$(33 + 67 i ) + (5 + 28 i)$ | $z_1 + z_2$
$(33 + 67 i ) + (5 + 28 i)$ | $(a_1 + b_1 i) + (a_2 + b_2 i)$
$33 + 67 i  + 5 + 28 i$ | $a_1 + b_1 i + a_2 + b_2 i$
$(33 + 5) + (67 + 28) i$| $(a_1 + a_2) + (b_1 + b_2) i$
$38 + 95 i$| $(a_1 + a_2) + (b_1 + b_2) i$

### Subtraktion
Beispiel | allgemeiner Fall
---	| ---
$(80 + 24 i ) - (48 + 76 i)$ | $z_1 - z_2$
$(80 + 24 i ) - (48 + 76 i)$ | $(a_1 + b_1 i) - (a_2 + b_2 i)$
$80 + 24 i  - 48 - 76 i$ | $a_1 + b_1 i - a_2 - b_2 i$
$(80 - 48) + (24 - 76) i$| $(a_1 - a_2) + (b_1 - b_2) i$
$32 - 52 i$| $(a_1 - a_2) + (b_1 - b_2) i$


