---
content_type: article
page_id: koZa_Einführung
page_name: Notwendigkeit einer Zahlenbereichserweiterung durch komplexe Zahlen
main_category: kat_komplex_Zahlen
sub_category: kat_komplex_Zahlen
requirements: 
exercise_path: 
---

# Notwendigkeit der Zahlenbereichserweiterung

Zum finden von Nullstellen in Quadratischen Gleichungen lässt sich die p-q-Formel verwenden.

Dabei können verschiedene Fälle auftreten:


|Gleichung                                                                                                               | $x^2 - 4x + 2$                                                                        | $x^2 - 4 x + 4$                                                                        | $x^2 - 4 x + 6$                                                                        |
| ------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- |
| **Skizze**                                                                                                    | ![](/static/mainapp/images/koZa_Einführung1.png) | ![](/static/mainapp/images/koZa_Einführung2.png) | ![](/static/mainapp/images/koZa_Einführung3.png) |
| **Nullstellenberechnung mit der p-q-Formel** : $x_{01/02} = - \frac {p}{ 2} \pm \sqrt{\left(\frac {p}{2}\right) ^2 - q}$ | $x_{01} = -\left(\frac {-4}{2}\right) + \sqrt {\left(\frac {-4}{2}\right)^2-2} = 2 + \sqrt{2}$              | $x_{01} = -\left(\frac {-4}{2}\right) + \sqrt {\left(\frac {-4}{2}\right)^2-4} = 2 + \sqrt{0}$              | $x_{01} = -\left(\frac {-4}{2}\right) + \sqrt {\left(\frac {-4}{2}\right)^2-6} = 2 + \sqrt{-2}$             |
|                                                                                                               | $x_{02} = -\left(\frac {-4}{2}\right) - \sqrt {\left(\frac {-4}{2}\right)^2-2} = 2 - \sqrt{2}$              | $x_{02} = -\left(\frac {-4}{2}\right) + \sqrt {\left(\frac {-4}{2}\right)^2-4} = 2 - \sqrt{0}$              | $x_{02} = -\left(\frac {-4}{2}\right) + \sqrt {\left(\frac {-4}{2}\right)^2-6} = 2 - \sqrt{-2}$             |
| **Bezeichnung**                                                                                                              | zwei echt verschiedene Nullstellen                                                                                       |  zwei identische Nullstellen                                                                                     | keine reellen Nullstellen                                                                                      |


In der 3. Gleichung tritt ein Problem auf: Der Therm $2 \pm \sqrt{-2}$ liefert keine Lösung innerhalb des Zahlenbereiches der reellen Zahlen, da sich die Wurzel von negativen Zahlen nicht berechnen lässt. Verzichtet man jedoch auf das Ausrechnen dieser Wurzeln, so ergibt eine Probe mit diesen Ergebnissen dennoch eine wahre Aussage:

$x^2 - 4x + 6$ mit $x_{01} = 2 + \sqrt{-2}$

$(2 + \sqrt{-2})^2 - 4(2 + \sqrt{-2}) + 6$

$\Leftrightarrow 4 + 2 \cdot 2 \cdot \sqrt{-2} - 2 - 8 - 4 \cdot \sqrt{2} + 6$

$\Leftrightarrow 4-2-8+6+4\cdot\sqrt{-2}-4\cdot\sqrt{-2}$

$\Leftrightarrow 0$

beziehungsweise

$x^2 - 4x + 6$ mit $x_{01} = 2 - \sqrt{-2}$

$(2 - \sqrt{-2})^2 - 4(2 - \sqrt{-2}) + 6$

$\Leftrightarrow 4 - 2 \cdot 2 \cdot \sqrt{-2} - 2 - 8 + 4 \cdot \sqrt{2} + 6$

$\Leftrightarrow 4-2-8+6-4\cdot\sqrt{-2}+4\cdot\sqrt{-2}$

$\Leftrightarrow 0$

Wenn man also annimmt, dass die Wurzeln von negativen Zahlen existieren, sind alle derartigen Gleichungen lösbar. Der Wert der negativen Wurzeln wird dabei durch die "komplexen Zahlen" auch genannt "imaginären Zahlen"  repräsentiert.
