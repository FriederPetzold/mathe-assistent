# Nomenclature:
## Content files:
>are located under /mainapp/content

### Overview pages:
- name stars with "Kat" (not needable, just formal)
- YAML-Header: 
```YAML
---
content_type: overview
level: 
upper_level: 
page_id:
page_name: 
---
```
- contains:
	- content_type: must be "overview"
	- level: a number from 0+ (Only pages with level 0 & 1 are used)
	- upper_level: if the level is 1+, the page_id from the upper-level-page should be in here.
	- page_id: the identifier of the page
	- page_name: the name of the page, which is shown in the menu

### Content pages:
- YAML-Header:
```YAML
---
content_type: article
page_id: 
page_name: 
main_category: 
sub_category: 
requirements: 
geneated_exercise: 
---
```

- contains:
	- content_type: must be "article"
	- page_id: the identifier of the page
	- page_name: the name of the page which is shown to the user
	- category: the category (Overview-page) the Side is listed in
	- requirements: a list of Articles (with their page_id) which are needet to understand this article
	- generated exercise: contains nothing if there is none, if there is one, it contains the name of the exercise-scripts wich is listed in /content/module

## Exercise generation modules:
> are located under /mainapp/content/modules

- need to contain a ``` getAufgabe()``` function, which returns the exercise as a string in Markdown (+ LaTeX)-Syntax

## nomenclature in the code:
- everything working with the generation of the application is named in English
	- multiple words are seperated by an "_"
- everything working with the content is named in German
	- mutiple words are seperated by setting the first letter of the second ord to upper case
